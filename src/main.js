import 'phaser';
import Init from './scenes/init';
import { gameWidth, gameHeight } from './const';

const config = {
  type: Phaser.AUTO,
  with: gameWidth,
  height: gameHeight,
  scene: Init,
  scale: { 
    mode: Phaser.Scale.FIT
  }
};

new Phaser.Game(config);