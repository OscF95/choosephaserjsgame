import player1 from '../../../assets/player1.png';
import player2 from '../../../assets/player2.png';
import startButton from '../../../assets/startButton.png';
import { textStyle } from '../../const';


class Init extends Phaser.Scene {
  preload() {
    this.gameStarted = false;
    this.gameOver = true;
    this.load.image('player1', player1);
    this.load.image('player2', player2);
    this.load.image('startButton', startButton);
  }
  create() {
    this.timeLimit = 30;
    this.timeCounter = this.timeLimit;

    this.player1 = this.add.sprite(250, 200, 'player1').setScale(1.2);
    this.player2 = this.add.sprite(750, 200, 'player2').setScale(1.2);
    this.startButton = this.add.sprite(480, 420, 'startButton');

    this.player1.setInteractive();
    this.player1.on('pointerdown', () => this.incrementPlayerScore(this.player1));

    this.player2.setInteractive();
    this.player2.on('pointerdown', () => this.incrementPlayerScore(this.player2));

    this.startButton.setInteractive();
    this.startButton.on('pointerdown', () => this.startGame());

    this.player1ScoreText = this.add.text(90, 50, this.player1Counter, textStyle);
    this.player2ScoreText = this.add.text(600, 50, this.player2Counter, textStyle);
    this.timerText = this.add.text(450, 300, this.timeCounter, textStyle);

    this.setPlayersScore();
  }
  update() {
    if (this.gameStarted) {
      this.gameStarted = false;
      this.gameOver = false;
      this.timerManager();
    }
  }

  incrementPlayerScore(face) {
    if (!this.gameOver) {
      if (face.texture.key === this.player1.texture.key) {
        this.player1Counter++;
        this.player1ScoreText.setText(this.player1Counter);
      } else if (face.texture.key === this.player2.texture.key) {
        this.player2Counter++;
        this.player2ScoreText.setText(this.player2Counter);
      }
    }
  }

  timerManager() {
    this.timeCounter--;
    this.timerText.setText(this.timeCounter);

    if (this.timeCounter === 0) {

      this.selectWinner();
      this.gameOver = true;
    } else {
      this.time.delayedCall(1000, this.timerManager, [], this);
    }
  }

  startGame() {
    if (this.gameOver) {
      this.gameStarted = true;
      this.timeCounter = this.timeLimit;
      this.setPlayersScore();
    }
  }

  selectWinner() {
    let message;
    if (this.player1Counter != this.player2Counter) {
      message = (this.player1Counter > this.player2Counter) ?
        `El jugador 1 ha ganado con ${this.player1Counter} puntos` :
        `El jugador 2 ha ganado con ${this.player2Counter} puntos`
    } else {
      message = "Esto es un empate"
    }
    message = `${message} \nPulsa el botón 'Start Game' para comenzar de nuevo`
    alert(message);
  }

  setPlayersScore() {
    this.player1Counter = 0;
    this.player2Counter = 0;
    this.player1ScoreText.setText(this.player1Counter);
    this.player2ScoreText.setText(this.player2Counter);
  }
}

export default Init;